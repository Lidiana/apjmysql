const express = require("express");
const bp = require("body-parser");
const mysql = require("mysql");
const path = require("path");
const bodyParser = require("body-parser");
const fupl = require("express-fileupload");
const app = express();
const port = 3000;
const {chamarIndex} = require('./routes/index');

const bd =  mysql.createConnection ({
    host: 'localhost',
    user: 'lid',
    password: 'senhateste1',
    database: 'cepel'
    // OBS: ALTERAR DADOS ANTES DE INICIAR O SERVIDOR
});

bd.connect((err) => {
    if(err){ throw err};
    console.log("Conexão com a base de dados estabelecida com sucesso.")
});

global.bd = bd; // variavel global necessária para usar 'bd' no index.js

app.set('port', process.env.port || port);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false })); // revisar uso
app.use(bodyParser.json()); // revisar uso
app.use(express.static(path.join(__dirname, '/public')));
app.use(fupl()); 

app.get('/', chamarIndex);

app.listen(port, () => {console.log('Servidor ativo na porta: ${port}')});
