package apJmysql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.sql.DataSource;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnBD {
	
	// para conectar ao banco de dados no servidor local
	private static DataSource getDs() {
		MysqlDataSource ds = new MysqlDataSource();
		
		
		//	NAO ESQUECER DE ALTERAR OS VALORES ANTES DE SUBIR NO GIT / MUDAR SERVIDOR MYSQL
		ds.setServerName("localhost");
		ds.setPortNumber(3306);
		ds.setDatabaseName("cepel");
		ds.setUser("#__usuário __#");
		ds.setPassword("#__senha__#");
		ds.setUseSSL(false);
		return ds;
	}

	// função para inserção de dados genéricos de teste. subtituir o nome da tabela quando foi associar a outro banco de dados
	public void inserirDados(String aerogerador, float vv, float vr, float vc) {
		try(Connection conn = getDs().getConnection();){
			PreparedStatement ps = conn.prepareStatement("INSERT INTO teste (aerogerador, vel_vento, potencia_real, potencia_calculada) VALUES (?, ?, ?, ?);");
			ps.setString(1, aerogerador);
			ps.setFloat(2, vv);
			ps.setFloat(3, vr);
			ps.setFloat(4, vc); 
			//não usar .setDouble porque não funcionará
			
			System.out.println(ps);
			ps.executeUpdate();
		} catch(SQLException e) {
			sqlExc(e);
		}
	}
	
	public static void sqlExc(SQLException ex) {
	    for (Throwable e: ex) {
	        if (e instanceof SQLException) {
	            e.printStackTrace(System.err);
	            System.err.println("SQLState: " + ((SQLException) e).getSQLState());
	            System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
	            System.err.println("Message: " + e.getMessage());
	            Throwable t = ex.getCause();
	            while (t != null) {
	                System.out.println("Cause: " + t);
	                t = t.getCause();
	            }
	        }
	    }
	}
}