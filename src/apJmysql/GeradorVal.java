package apJmysql;

import java.util.Random;

public class GeradorVal {
	
	Random rdm = new Random();
	public static final String aerogerador = "AERO 1";
	
	// iniV = valor inicial do intervalo | finV = valor final do intervalo
	public int geradorInt(int iniV, int finV) {
		return rdm.nextInt(finV-iniV)+iniV+1;
	}
	
	// OBS: o formato de inserção no BD formata o valor float retornado desta função de y.xxxx para y.xx
	public float geradorFloat(int iniV, int finV) {
		return rdm.nextInt(finV-iniV)+iniV+rdm.nextFloat();
	}
}
